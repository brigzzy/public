#!/bin/bash
kubectl -n public-site create secret generic regcred \
    --from-file=.dockerconfigjson=/home/brigzzy/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson