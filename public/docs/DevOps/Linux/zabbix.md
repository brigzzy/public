# Zabbix

## Monitor a SystemD Service with Zabbix

[https://github.com/MogiePete/zabbix-systemd-service-monitoring](https://github.com/MogiePete/zabbix-systemd-service-monitoring)

If you have a service you want to monitor, edit the /etc/zabbix/service_discovery_whitelist file on the server, and restart the agent
