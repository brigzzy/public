# Keyboard Shortcuts

To add a shortcut to the keyboard shortcuts, you need to use this command:

`qvm-run -q -a personal "gonme-terminal --hide-menubar"`