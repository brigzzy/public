# Startup Applications

In the template (not the qube you run normally), put a desktop file in:

`~/.config/autostart/`

You can find the desktop files in `/usr/share/applications`