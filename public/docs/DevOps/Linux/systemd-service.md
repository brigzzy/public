# SystemD Services

My laptop has a strange issues where the DNS servers don't work after suspending, unless I ping them.  This systemd service pings them every 5 seconds.

/etc/systemd/system/ping-all-the-time.service
```
[Unit]
Description=Ping DNS Servers to keep them working

[Service]
Type=simple
RemainAfterExit=yes
ExecStart=/usr/local/bin/ping-all-the-time.sh
StandardOutput=null

[Install]
WantedBy=multi-user.target
```

And the script:

/usr/local/bin/ping-all-the-time.sh
```
#!/bin/bash

while true; do
  echo "ping 1.2.3.4 at `date`"
  ping -W 2 -c 1 1.2.3.4 -q > /dev/null
  echo "ping 5.6.7.8 at `date`"
  ping -W 2 -c 1 5.6.7.8 -q > /dev/null
  sleep 5;
done
```

Once it's all set up, reload the systemd daemon

`sudo systemctl daemon-reload`

And enable your service

`sudo systemctl enable ping-all-the-time.service`