# Bitbucket Pipelines

## Configuring a Bitbucket Pipeline Runner

Bitbucket Runners are really cool, it's the Bitbucket CI/CD thing.  You can self host the runner, and still keep your code in the cloud, which is pretty great.  

1. Start by logging into Bitbucket, and going to Your Profile > All Workspaces > Manage (on your workspace) > Workspace Runners
1. Add a new runner, and follow the steps to set it up.  Note, I needed to use Debian 10, not Debian 11, as there were some issues with Docker on Debian 11.  There's instructions out there for hosting the runner on K8S, but I had trouble get them working.
1. If you're going to interact with secrets, you can add them as environment vars on the bitbucket side by going to Your Profile > All Workspaces > Manage (on your workspace) > Workspace Variables (note you can also do this at the repo level too, if you don't want them to be project wide)
1. Once that's done, you'll need a `bitbucket-pipelines.yml` file in the root of your repo.  Here's an example of what that pipeline will look like:

```
image: python

options:
  docker: true

pipelines:
  branches:
    release:
      - step:
          name: Build
          runs-on: self.hosted
          script:
            - docker build -t brigzzy/image:v1 .
            - docker login -u $DOCKER_HUB_USERNAME -p $DOCKER_HUB_PASSWORD
            - docker push brigzzy/image:v1
          services:
            - docker
      - step:
          name: Deploy
          runs-on: self.hosted
          image: atlassian/pipelines-kubectl
          script:
              # base64 encoded kubeconfig file
            - echo $KUBE_CONFIG | base64 -d > kubeconfig.yml
            - kubectl --kubeconfig=kubeconfig.yml apply -f build/kubernetes/.
            - kubectl --kubeconfig=kubeconfig.yml rollout restart -f build/kubernetes/deployment.yaml -n my-namespace
            - kubectl --kubeconfig=kubeconfig.yml rollout status deployment my-app-deployment -n my-namespace

```
