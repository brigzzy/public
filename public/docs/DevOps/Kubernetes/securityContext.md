# Security Context

When you set up a deployment, you can append a securityContext to your containers.  You can use a security context to lock down your containers.

```
spec:
  template:
    spec:
      containers:
      - name: your-container
        securityContext:
          runAsUser: 1001
          runAsNonRoot: true
          readOnlyRootFilesystem: true
          allowPrivilegeEscalation: false
          capabilities:
            drop:
            - all
            add:
            - NET_BIND_SERVICE
```

* runAsUser: Sets the UID to run as.  Note this doesn't need to exist on the nodes or anything, just set it to an arbetrary number
* runAsNonRoot: True or False.  Self Explanitory
* readOnlyRootFilesystem: True or False.  Self Explanitory
* allowPrivilegeEscalation: True or False.  Lets you use sudo or something similar to elevate your privileges within the container.  
* capabilities: You would usually want to drop all capabilities, and just add the ones you need.  NET_BIND_SERVICE is almost always needed if you're binding to a port.  [Here's a list of the Kubernetes capabilities](https://unofficial-kubernetes.readthedocs.io/en/latest/concepts/policy/container-capabilities/), and how they map to [Linux capabilities](https://man7.org/linux/man-pages/man7/capabilities.7.html)