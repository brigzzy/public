# MetalLB

## Asumptions

* You're running MicroK8s
* You have an IP address range to assign to MetalLB

## Enabling on MicroK8s

From one of the nodes run this:

`/snap/bin/microk8s enable metallb`

Give it an IP range to use

## Simple Example

Here's how you use it:

```
---
apiVersion: v1
kind: Namespace
metadata:
  name: test123

---
apiVersion: v1
kind: Service
metadata:
  name: nginx-ingress
  namespace: test123
spec:
  selector:
    app: nginx
  type: LoadBalancer
  loadBalancerIP: 10.90.50.200
  ports:
  - name: http
    port: 80
    targetPort: 80
  - name: https
    port: 443
    targetPort: 80

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: test123
  labels:
    app: nginx
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```