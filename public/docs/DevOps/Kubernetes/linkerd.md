# Linkerd

Linkerd is a lightweight service mesh for pod level monitoring.

## Installing

[See the Docs](https://linkerd.io/2.10/getting-started/)

## Viewing the Dashboard

Needs to be installed:

`linkerd viz install | kubectl apply -f -`

Once it's installed this will run the proxy so you can connect

`linkerd viz dashboard &`

The connect by going to [http://localhost:50750](http://localhost:50750)

## Automatic Injection

Edit your namespace yaml, and add `linkerd.io/inject: enabled` to the annotations:

```
apiVersion: v1
kind: Namespace
metadata:
  name: your-rockin-namespace
  annotations:
    linkerd.io/inject: enabled
```

## Monitoring a Deployment

`kubectl get deploy -o yaml -n your-rockin-namespace | linkerd inject - | kubectl apply -f -`

## Un-Monitoring a Deployment

`kubectl get deploy -o yaml -n your-rockin-namespace | linkerd uninject - | kubectl apply -f -`