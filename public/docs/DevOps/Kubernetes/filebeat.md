# Filebeat

## Filebeat Sidecar to ship logs to Elastic

The important thing is to have a common VolumeMount between the two containers, and to have the FileBeat sidecar use the configmap.  It would also be good to find a way to use a secret instead of a hard coded password in the configmap.

Filebeat Configmap:
```
apiVersion: v1
kind: ConfigMap
metadata:
  name: filebeat-configmap
  namespace: your-namespace
  labels:
    app: your-app
data:
  filebeat.yml: |
    filebeat:
      config:
        modules:
          path: /usr/share/filebeat/modules.d/*.yml
          reload:
            enabled: true
      modules:
      - module: nginx
        access:
          var.paths: ["/var/log/nginx/access.log*"]
        error:
          var.paths: ["/var/log/nginx/error.log*"]
      - module: apache
        access:
          enabled: true
          var.paths: ["/usr/local/apache2/logs/access.log"]
        error:
          enabled: true
          var.paths: ["/usr/local/apache2/logs/error.log"]
    output:
      elasticsearch:
        hosts: ["<YOUR_ELASTIC_IP>:9200"]
        username: elastic
        password: <YOUR_GOOD_PASSWORD_HERE>
```
Deployment Config (Your App's deployment, not a new one for Filebeat.  Sidecars attach to existing pods):
```
spec:
...
      containers:
      - name: your-app
...
        volumeMounts:
          - name: apache-logs
            mountPath: /usr/local/apache2/logs/
...
      - name: filebeat-sidecar
        image: docker.elastic.co/beats/filebeat:7.15.0
        volumeMounts:
          - name: apache-logs
            mountPath: /usr/local/apache2/logs/
          - name: filebeat-config
            mountPath: /usr/share/filebeat/filebeat.yml
            subPath: filebeat.yml
      volumes:
        - name: apache-logs
        - name: filebeat-config
          configMap:
            name: filebeat-configmap
            items:
              - key: filebeat.yml
                path: filebeat.yml
...
```
