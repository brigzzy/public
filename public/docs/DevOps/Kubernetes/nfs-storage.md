# NFS Storage

## Persistent Storage with NFS

[https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner/blob/master/charts/nfs-subdir-external-provisioner/README.md](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner/blob/master/charts/nfs-subdir-external-provisioner/README.md)

TL;DR

* Install the nfs-common package on the Kubernetes nodes
* Make sure the nodes are in the /etc/exports file on the nfs server so they're allowed to connect:

`/nfs 192.168.1.0/24(rw,root_squash,subtree_check)`

* Install Helm on your local
* Add the repo:

`helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/`

* Install the Chart:

`helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --set nfs.server=<NFS_SERVER_IP> --set nfs.path=<NFS_PATH> --set replicaCount=2`
the-world-anew