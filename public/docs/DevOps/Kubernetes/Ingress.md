# How to Set Up an Ingress with TLS

1. Start by installing your ingress controller.  I'm using the default nginx ingress controller that's included as an add on in MicroK8S.  Enable it by running `microk8s enable ingress`
1. Once that's done install the latest version of cert manager, per their [installation instructions](https://cert-manager.io/docs/installation/)
1. You can now create a secret for your Cloudflare API key:

```
---
apiVersion: v1
kind: Secret
metadata:
  name: cloudflare-api-token-secret
  namespace: cert-manager
type: Opaque
stringData:
  api-token: <TOKEN GOES HERE>
```

4. Once that's done, you need to create your ClusterIssuer:

```
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-staging
  namespace: cert-manager
spec:
  acme:
    email: <YOUR EMAIL GOES HERE>
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: staging-issuer-account-key
    solvers:
    - dns01:
        cloudflare:
          email: <YOUR EMAIL GOES HERE>
          apiTokenSecretRef:
            name: cloudflare-api-token-secret
            key: api-token
```
5. Do the same for prod:

```
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-production
  namespace: cert-manager
spec:
  acme:
    email: <YOUR EMAIL GOES HERE>
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: production-issuer-account-key
    solvers:
    - dns01:
        cloudflare:
          email: <YOUR EMAIL GOES HERE>
          apiTokenSecretRef:
            name: cloudflare-api-token-secret
            key: api-token
```

6. Finally, you can create your ingress object:

```
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: your-app-ingress
  namespace: your-app
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt-production
    kubernetes.io/tls-acme: "true"
    cert-manager.io/common-name: <YOUR DOMAIN>
spec:
  tls:
  - hosts:
    - <YOUR DOMAIN>
    secretName: your-app-tls-acme
  rules:
  - host: <YOUR DOMAIN>
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: your-app-service
            port:
              number: 8080
```